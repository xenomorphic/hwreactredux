import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './login.jsx';
import Registration from './Registration.jsx';
import NotFoundPage from './notfoundpage.jsx';
import Home from './home.jsx';
import reducer from '../Store/Reducers';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
function App() {

  const store = createStore(reducer);

  return (
      <div className="auth-wrapper">
          <div className="auth-inner">
              <Provider store={store}>
                <Router>
                      <Routes>
                      <Route path="/Login" element={<Login />} />
                      <Route path="/Registration" element={<Registration />} />
                      <Route path="*" element={<NotFoundPage />} />
                      <Route path="/" element={<Home />} />
                      </Routes>
                  </Router>
              </Provider>
          </div>
      </div>
  );
}

export default App;
